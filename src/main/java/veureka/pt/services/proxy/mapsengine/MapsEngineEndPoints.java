package veureka.pt.services.proxy.mapsengine;

import android.content.Context;

import veureka.pt.services.AException;
import veureka.pt.services.proxy.retrofit.CredentialManager;
import veureka.pt.services.proxy.retrofit.IServicesFactory;
import veureka.pt.services.proxy.retrofit.RetrofitProxy;

enum MapsEngineEndPoints implements IServicesFactory<MapsEngineProxyEndPoint> {
    MapsEngineProxyEndPoint(MapsEngineProxyEndPoint.class);
    private MapsEngineProxyEndPoint endPoint;
    private Class<MapsEngineProxyEndPoint> serviceClass;
    
    MapsEngineEndPoints(Class<MapsEngineProxyEndPoint> clazz) {
        this.serviceClass = clazz;
    }
    
    @Override
    public Class<MapsEngineProxyEndPoint> getServiceClass() {
        return this.serviceClass;
    }
    
    @Override
    public MapsEngineProxyEndPoint getServices() {
        return this.endPoint;
    }
    
    @Override
    public MapsEngineProxyEndPoint startEndPoint(Context context, String username, String password, String baseURL)
            throws AException {
        CredentialManager credentialManager = MapsEngineCredential.getInstance();
        credentialManager.setUsername(username);
        credentialManager.setPassword(password);
        this.endPoint = RetrofitProxy.createEndPoint(context, this, false);
        return this.endPoint;
    }
    
    @Override
    public MapsEngineProxyEndPoint startEndPoint(Context context, String token, String baseURL) throws AException {
        if(token != null && !token.isEmpty()){
    
            MapsEngineCredential credentials = MapsEngineCredential.getInstance();
            if(credentials!=null && token != null) {
                credentials.setAuthToken(token);
                this.endPoint = RetrofitProxy.createEndPoint(context, this, true);
            }
        }
    
        return this.endPoint;
    }
    
    @Override
    public CredentialManager getCredentials() {
        return MapsEngineCredential.getInstance();
    }
    
    
    
}
