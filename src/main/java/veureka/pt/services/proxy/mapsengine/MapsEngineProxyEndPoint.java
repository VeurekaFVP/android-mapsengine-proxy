package veureka.pt.services.proxy.mapsengine;

import com.google.gson.JsonObject;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

@SuppressWarnings("unused")
interface MapsEngineProxyEndPoint {

    @GET("place/autocomplete/json")
    Single<JsonObject> searchPlaceAutocomplete(@QueryMap Map<String, String> options);

    @GET("geocode/json")
    Single<JsonObject> searchGeocode(@QueryMap Map<String, String> options);

    @GET("directions/json")
    Single<JsonObject> searchDirections(@QueryMap Map<String, String> options);

    @GET("place/details/json")
    Single<JsonObject> getDetails(@QueryMap Map<String, String> options);

    @GET("distancematrix/json")
    Single<JsonObject> getDistanceMatrix(@QueryMap Map<String, String> options);
}
