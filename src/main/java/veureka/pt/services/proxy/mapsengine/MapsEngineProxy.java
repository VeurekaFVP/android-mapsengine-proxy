package veureka.pt.services.proxy.mapsengine;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import veureka.pt.services.AException;
import veureka.pt.services.AndroidException;

public class MapsEngineProxy {
    
    private static final String MAPS_ERROR_LOCATION_ENABLED = "maps_error_location_enabled";
    private static final String MAPS_ERROR_GET_CURRENT_LOCATION = "maps_error_get_current_location";
    private static final String DRIVING_TRAVEL_MODE = "driving";
    private static final String WALKING_TRAVEL_MODE = "walking";
    private static final String BICYCLING_TRAVEL_MODE = "bicycling";
    private static final String TRANSIT_TRAVEL_MODE = "transit";
    
    private final Context mContext;
    private GoogleAPIProxy mGoogleAPIProxy;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mGoogleMapFragment;
    private FusedLocationProviderClient mFusedLocationClient;
    private HashMap<Marker, Runnable> mOnMarkerClickListeners;
    
    public MapsEngineProxy(Context context) throws AException {
        this.mContext = context;
        mOnMarkerClickListeners = new HashMap<>();
        mGoogleAPIProxy = GoogleAPIProxy.createInstance(context);
    }
    
    public Single<Boolean> setGoogleMapFragment(SupportMapFragment googleMapFragment) {
        this.mGoogleMapFragment = googleMapFragment;
        return Single.create(new SingleOnSubscribe<Boolean>() {
            @Override
            public void subscribe(@NonNull final SingleEmitter e) throws Exception {
                mGoogleMapFragment.getMapAsync(new OnMapReadyCallback() {
                    
                    @Override
                    @SuppressWarnings("unchecked")
                    public void onMapReady(GoogleMap googleMap) {
                        mGoogleMap = googleMap;
                        e.onSuccess(true);
                    }
                });
            }
        }).subscribeOn(AndroidSchedulers.mainThread())
                     .observeOn(AndroidSchedulers.mainThread());
    }
    
    
	public Single<GoogleMap> setGoogleMapFragmentObject(SupportMapFragment googleMapFragment) {
        this.mGoogleMapFragment = googleMapFragment;
        return Single.create(new SingleOnSubscribe<GoogleMap>() {
            @Override
            public void subscribe(@NonNull final SingleEmitter e) throws Exception {
                mGoogleMapFragment.getMapAsync(new OnMapReadyCallback() {
                    
                    @Override
                    @SuppressWarnings("unchecked")
                    public void onMapReady(GoogleMap googleMap) {
                        mGoogleMap = googleMap;
                        e.onSuccess(mGoogleMap);
                    }
                });
            }
        }).subscribeOn(AndroidSchedulers.mainThread())
                     .observeOn(AndroidSchedulers.mainThread());
    }
    
@SuppressWarnings("unused")
    public Marker createMarker(double lat, double lon, String titulo, Integer imageResource) {
        return createMarker(new LatLng(lat, lon), titulo, imageResource);
    }
    
    public void disableOnMarkerClickListener() {
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }
    
    public void enableCustomOnMarkerClickListener() {
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Runnable run = mOnMarkerClickListeners.get(marker);
                if(run != null) {
                    run.run();
                } else {
                    return false;
                }
                return true;
            }
        });
    }
    
    public void addOnClickMarkerListener(Marker key, Runnable onClick) {
        if(onClick != null) {
            mOnMarkerClickListeners.put(key, onClick);
        }
    }
    
    @SuppressWarnings("WeakerAccess")
    public Marker createMarker(LatLng coordinate, String titulo, Integer imageResource) {
        Marker marker = null;
        if(mGoogleMap != null) {
            if(imageResource == null) {
                marker = mGoogleMap.addMarker(new MarkerOptions().position(coordinate).title(titulo));
            } else {
                marker = mGoogleMap.addMarker(new MarkerOptions().position(coordinate)
                                                                 .title(titulo)
                                                                 .icon(BitmapDescriptorFactory.fromResource(
                                                                         imageResource)));
            }
        }
        return marker;
    }
    
    public void setCamera(LatLng coordinate) {
        if(mGoogleMap != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
        }
    }
    
    @SuppressWarnings("unused")
    public void setCamera(LatLng coordinate, Float zoom) {
        if(mGoogleMap != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, zoom));
        }
    }
    
    public void setCamera(LatLng origin, LatLng destination) {
        LatLngBounds bounds = LatLngBounds.builder().include(origin).include(destination).build();
        if(mGoogleMap != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
        }
    }
    
    public void setCamera(final String points) {
        
        final int routePadding = 100;
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        List<LatLng> latLngPoints = new PolylineOptions().addAll(PolyUtil.decode(points)).getPoints();
        for(LatLng point : latLngPoints) {
            boundsBuilder.include(point);
        }
        final LatLngBounds latLngBounds = boundsBuilder.build();
        Handler mainHandler = new Handler(mContext.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
            }
        });
    }
    
    public void setZoom(Float zoom) {
        if(zoom != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(zoom));
        }
    }
    
    @SuppressWarnings("unused")
    public Marker showMarker(double lat, double lon, String titulo, Float zoom) {
        return showMarker(new LatLng(lat, lon), titulo, zoom);
    }
    
    public Marker showMarker(LatLng coordinate, String titulo, Float zoom) {
        Marker marker = createMarker(coordinate, titulo, null);
        setCamera(coordinate);
        setZoom(zoom);
        return marker;
    }
    
    public Marker showMarker(LatLng coordinate, String titulo, Float zoom, Integer imageResource) {
        Marker marker = createMarker(coordinate, titulo, imageResource);
        setCamera(coordinate);
        setZoom(zoom);
        return marker;
    }
    
    @SuppressWarnings("unused")
    public void cleanMap() {
        if(mGoogleMap != null) {
            mGoogleMap.clear();
        }
    }
    
    private void addPolyLine(final String points, int colorR) {
        if(mGoogleMap != null) {
            Handler mainHandler = new Handler(mContext.getMainLooper());
            final PolylineOptions lineOptions = new PolylineOptions().addAll(PolyUtil.decode(points));
            lineOptions.color(ContextCompat.getColor(mContext, colorR));
            
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            List<LatLng> latLngPoints = new PolylineOptions().addAll(PolyUtil.decode(points)).getPoints();
            for(LatLng point : latLngPoints) {
                boundsBuilder.include(point);
            }
            final LatLngBounds latLngBounds = boundsBuilder.build();
            final int routePadding = 100;
            
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    mGoogleMap.addPolyline(lineOptions);
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
                }
            });
        }
    }

    public Single<String> getPoints(final LatLng origin, final LatLng destination) throws AException {

        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(final SingleEmitter<String> e) throws Exception {
                mGoogleAPIProxy.getDirectionPath(mContext, origin, destination, DRIVING_TRAVEL_MODE)
                        .onErrorResumeNext(mGoogleAPIProxy.getDirectionPath(mContext, origin, destination, WALKING_TRAVEL_MODE)
                                .onErrorResumeNext(mGoogleAPIProxy.getDirectionPath(mContext, origin, destination, TRANSIT_TRAVEL_MODE)
                                        .onErrorResumeNext(mGoogleAPIProxy.getDirectionPath(mContext, origin, destination, BICYCLING_TRAVEL_MODE)
                                                .doOnError(new Consumer<Throwable>() {
                                                    @Override
                                                    public void accept(Throwable throwable) throws Exception {
                                                        e.onError(throwable);
                                                    }
                                                }))))
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String string) throws Exception {
                                if(string != null){
                                    e.onSuccess(string);
                                }else{
                                    e.onError(new Throwable());
                                }
                            }
                        });
            }
        });
        //return mGoogleAPIProxy.getDirectionPath(mContext, origin, destination);
    }
    
    public Single<Double> getDistance(LatLng origin, LatLng destination) throws AException {
        return getPoints(origin, destination).map(new Function<String, Double>() {
            @Override
            public Double apply(@NonNull String points) throws Exception {
                return SphericalUtil.computeLength(PolyUtil.decode(points));
            }
        });
    }

    public Single<HashMap> getDistanceMatrix(final LatLng origin, final LatLng destination) throws AException {
        return Single.create(new SingleOnSubscribe<HashMap>() {
            @Override
            public void subscribe(final SingleEmitter<HashMap> e) throws Exception {
                mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination) /*,DRIVING_TRAVEL_MODE)
                        .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, WALKING_TRAVEL_MODE)
                            .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, TRANSIT_TRAVEL_MODE)
                                    .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, BICYCLING_TRAVEL_MODE)
                                            .doOnError(new Consumer<Throwable>() {
                                                @Override
                                                public void accept(Throwable throwable) throws Exception {
                                                    System.out.println("DISTANCEMATRIX - throwable:"+throwable);
                                                    e.onError(throwable);
                                                }
                                            }))))*/
                        .subscribe(new Consumer<HashMap>() {
                            @Override
                            public void accept(HashMap data) throws Exception {
                                if (data != null) {
                                    e.onSuccess( data);
                                } else {
                                    e.onError(new Throwable());
                                }
                            }
                        });
            }
        });
    }

    public Single<HashMap> getDistanceMatrix(final String origin, final LatLng destination) throws AException {
        return Single.create(new SingleOnSubscribe<HashMap>() {
                                 @Override
                                 public void subscribe(final SingleEmitter<HashMap> e) throws Exception {
                                     mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination)/*, DRIVING_TRAVEL_MODE)
                                             .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, WALKING_TRAVEL_MODE)
                                                     .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, TRANSIT_TRAVEL_MODE)
                                                             .onErrorResumeNext(mGoogleAPIProxy.getDistanceMatrix(mContext, origin, destination, BICYCLING_TRAVEL_MODE)*/
                                             .doOnError(new Consumer<Throwable>() {
                                                 @Override
                                                 public void accept(Throwable throwable) throws Exception {
                                                     e.onError(throwable);
                                                 }
                                             })
                                             .subscribe(new Consumer<HashMap>() {
                                                 @Override
                                                 public void accept(HashMap data) throws Exception {
                                                     if (data != null) {
                                                         e.onSuccess(data);
                                                     } else {
                                                         e.onError(new Throwable());
                                                     }
                                                 }
                                             });
                                 }
                             }
        );
    }
    
    public Single<Double> getRouteDirection(LatLng origin, LatLng destination, final int colorR)
            throws AException {
        
        return getPoints(origin, destination).map(new Function<String, Double>() {
            @Override
            public Double apply(@NonNull String points) throws Exception {
                addPolyLine(points, colorR);
                return SphericalUtil.computeLength(PolyUtil.decode(points));
            }
        });
    }
    
    @SuppressWarnings("unused")
    public void setMyLocationEnabled(Boolean enabled) throws AException {
        try {
            if(mGoogleMap != null) {
                mGoogleMap.setMyLocationEnabled(enabled);
            }
        } catch(SecurityException e) {
            throw AndroidException.make(mContext, MAPS_ERROR_LOCATION_ENABLED, e);
        }
    }
    
    @SuppressWarnings("unused")
    private void removeListener(LocationManager locationManager, android.location.LocationListener locationListener) {
        locationManager.removeUpdates(locationListener);
    }
    
    public void setOnMapClickListener(final Single singleTaskObserver) {
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                singleTaskObserver.subscribe();
            }
        });
    }
    
    public void setScrollGesturesEnabled(boolean flag) {
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(flag);
    }
    
    public void setRotateGesturesEnabled(boolean flag) {
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(flag);
    }
    
    @SuppressWarnings("unused")
    public void setZoomControlsEnabled(boolean flag) {
        mGoogleMap.getUiSettings().setZoomControlsEnabled(flag);
    }
    
    public void setZoomGesturesEnabled(boolean flag) {
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(flag);
    }
    
    public Single<LatLng> getCurrentLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        return Single.create(new SingleOnSubscribe<LatLng>() {
            @Override
            public void subscribe(@NonNull final SingleEmitter<LatLng> singleEmitter) throws Exception {
                try {
                    mFusedLocationClient.getLastLocation()
                                        .addOnSuccessListener(new OnSuccessListener<Location>() {
                                            @Override
                                            public void onSuccess(Location location) {
                                                // Got last known location. In some rare situations this can be null.
                                                if(location != null) {
                                                    LatLng latlng = new LatLng(location.getLatitude(),
                                                                               location.getLongitude());
                                                    singleEmitter.onSuccess(latlng);
                                                } else {
                                                    singleEmitter.onError(AndroidException.make(mContext, MAPS_ERROR_GET_CURRENT_LOCATION, new NullPointerException()));
                                                }
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                singleEmitter.onError(e);
                                            }
                                        });
                } catch(SecurityException ex) {
                    singleEmitter.onError(AndroidException.make(mContext, MAPS_ERROR_GET_CURRENT_LOCATION, ex));
                }
            }
        });
    }
}
