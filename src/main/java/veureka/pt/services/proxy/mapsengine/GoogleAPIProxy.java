package veureka.pt.services.proxy.mapsengine;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import veureka.pt.services.AException;
import veureka.pt.services.BundleManager;
import veureka.pt.services.proxy.retrofit.RetrofitProxy;
import java.util.concurrent.Callable;

public class GoogleAPIProxy {

    private static GoogleAPIProxy sInstance = null;
    @SuppressWarnings("unused")
    public static final String GEOCODE_FILTER = "geocode";
    @SuppressWarnings("unused")
    public static final String ADDRESS_FILTER = "address";
    @SuppressWarnings("unused")
    public static final String ESTABLISHMENT_FILTER = "establishment";
    @SuppressWarnings("unused")
    public static final String REGIONS_FILTER = "(regions)";
    public static final String CITIES_FILTER = "(cities)";

    public static GoogleAPIProxy createInstance(Context context) throws AException {
        if(sInstance == null) {
            for(ServiceRequestType serviceRequestType : ServiceRequestType.values()) {
                serviceRequestType.startEndPoint(context, null, null, null);
            }
            String API_KEY_REF = "com.google.android.geo.API_KEY";
            String apiKey = BundleManager.getInstance().getMetadata(context, API_KEY_REF);
            sInstance = new GoogleAPIProxy(apiKey);
        }
        return sInstance;
    }

    private final String mGoogleAPIKey;

    private GoogleAPIProxy(String apiKey) {
        this.mGoogleAPIKey = apiKey;
    }

    @SuppressWarnings("WeakerAccess")
    public Single<String> getDirectionPath(Context context, final LatLng origin, final LatLng destination, String travelMode)
            throws AException {
        Map<String, String> query = new HashMap<>();
        query.put("origin", origin.latitude + "," + origin.longitude);
        query.put("destination", destination.latitude + "," + destination.longitude);
        if(travelMode != null){
            query.put("mode", travelMode);
        }
        return handle(context, ServiceRequestType.SearchDirections, null, query)
                .map(new Function<JsonObject, String>() {
                    @Override
                    public String apply(@NonNull JsonObject jsonObject) {
                        if(!jsonObject.isJsonNull() && jsonObject.get("routes").getAsJsonArray().size() > 0) {
                            JsonObject routes = jsonObject.get("routes").getAsJsonArray()
                                                          .get(0).getAsJsonObject();
                            JsonObject overview = routes.get("overview_polyline").getAsJsonObject();
                            return overview.get("points").getAsString();
                        }
                        return "";
                    }
                });
    }

    @SuppressWarnings("WeakerAccess")
    public Single<HashMap> generalDistanceMatrix(Context context, String origin, LatLng destination, String travelMode)throws AException {
        Map<String, String> query = new HashMap<>();
        query.put("origins", origin);
        query.put("destinations", destination.latitude +","+ destination.longitude);

        if(travelMode!=null) query.put("mode", travelMode);

        return handle(context, ServiceRequestType.GetDistanceMatrix, null, query)
                .map(new Function<JsonObject, HashMap > () {
                    @Override
                    public HashMap apply(@NonNull JsonObject jsonObject){

                        HashMap<String, Object> response = new HashMap<>();
                        if(!jsonObject.isJsonNull() && jsonObject.get("rows").getAsJsonArray().get(0).getAsJsonObject().get("elements").getAsJsonArray().get(0).getAsJsonObject().get("status").getAsString().equals("OK") ) {
                            response.put("name", jsonObject.get("origin_addresses").getAsString());
                            response.put("distance", jsonObject.get("rows").getAsJsonArray().get(0).getAsJsonObject().get("elements").getAsJsonArray().get(0).getAsJsonObject().get("distance").getAsJsonObject().get("value").getAsDouble()/1000.0);
                        }
                        return response;
                    }
                });
    }

    public Single<HashMap> getDistanceMatrix(Context context, LatLng origin, LatLng destination, String travelMode)throws AException{
        return generalDistanceMatrix(context, origin.latitude +","+ origin.longitude, destination, travelMode);
    }

    public Single<HashMap> getDistanceMatrix(Context context, String originPlaceID, LatLng destination, String travelMode)throws AException{
        return generalDistanceMatrix(context, "place_id:"+originPlaceID, destination, travelMode);
    }

    public Single<HashMap> getDistanceMatrix(Context context, LatLng origin, LatLng destination)throws AException{
        return generalDistanceMatrix(context, origin.latitude +","+ origin.longitude, destination, null);
    }

    public Single<HashMap> getDistanceMatrix(Context context, String originPlaceID, LatLng destination)throws AException{
        return generalDistanceMatrix(context, "place_id:"+originPlaceID, destination, null);
    }

    public Single<LatLng> getCoordinateByText(Context context, String text, Map<String, String> queryParams)
            throws AException {
        if(queryParams == null) {
            queryParams = new HashMap<>();
        }
        queryParams.put("address", text);
        return handle(context, ServiceRequestType.SearchGeocode, null, queryParams)
                .map(new Function<JsonObject, LatLng>() {
                    @Override
                    public LatLng apply(@NonNull JsonObject jsonObject) throws Exception {
                        if(!jsonObject.isJsonNull() ) {
                            JsonObject location = jsonObject.get("results").getAsJsonArray()
                                                            .get(0).getAsJsonObject()
                                                            .get("geometry").getAsJsonObject()
                                                            .get("location").getAsJsonObject();
                            return new LatLng(location.get("lat").getAsFloat(), location.get("lng").getAsFloat());
                        }
                        return null;
                    }
                });
    }

    public Single<LatLng> getCoordinateByPlaceId(final Context context, String placeId, String language) throws AException {
        final Map<String, String> subQueryParams = new HashMap<String, String>();
        subQueryParams.put("placeid", placeId);
        subQueryParams.put("language", language);
        subQueryParams.put("fields", "geometry");
        return handle(context, ServiceRequestType.GetDetails, null, subQueryParams)
                .map(new Function<JsonObject, LatLng>() {
                    @Override
                    public LatLng apply(JsonObject jsonObject) throws Exception {
                        if(!jsonObject.isJsonNull()) {
                            JsonObject location = jsonObject.get("result").getAsJsonObject()
                                    .get("geometry").getAsJsonObject()
                                    .get("location").getAsJsonObject();
                            return new LatLng(location.get("lat").getAsFloat(), location.get("lng").getAsFloat());
                        }
                        return null;
                    }
                });
    }

    public Single<JsonObject> getDetailsWithGeometryAndAddress(Context context, final String placeId, String language, String sessionToken) throws AException {
        Map<String, String> subQueryParams = new HashMap<String, String>();
        subQueryParams.put("placeid", placeId);
        if (language != null)
            subQueryParams.put("language", language);
        subQueryParams.put("sessiontoken", sessionToken);
        subQueryParams.put("fields", "address_components,geometry");
        return handle(context, ServiceRequestType.GetDetails, null, subQueryParams)
                .map(new Function<JsonObject, JsonObject>() {
                    @Override
                    public JsonObject apply(@NonNull JsonObject jsonResultRequest) throws Exception {
                        JsonObject result = new JsonObject();
                        JsonArray detailResult = jsonResultRequest.get("result").getAsJsonObject().get("address_components").getAsJsonArray();
                        String cityName = detailResult.get(0).getAsJsonObject().get("long_name").getAsString();
                        String stateName = detailResult.get(1).getAsJsonObject().get("long_name").getAsString();
                        String countryName = detailResult.get(2).getAsJsonObject().get("long_name").getAsString();

                        JsonObject location = jsonResultRequest.get("result").getAsJsonObject().get("geometry").getAsJsonObject().get("location").getAsJsonObject();

                        result.addProperty("ciudad",cityName);
                        result.addProperty("estado",stateName);
                        result.addProperty("pais",countryName);
                        result.addProperty("lat", location.get("lat").getAsFloat());
                        result.addProperty("lng", location.get("lng").getAsFloat());
                        return result;
                    }
                });
    }

    public Single<String> getCityNameByLatLng(Context context, final LatLng coordenadas) throws AException{
        Map<String, String> subQueryParams = new HashMap<String, String>();
        subQueryParams.put("latlng", coordenadas.latitude+","+coordenadas.longitude);
        subQueryParams.put("result_type","locality");
        subQueryParams.put("language", "es");
        return handle(context, ServiceRequestType.SearchGeocode, null, subQueryParams)
                .map(new Function<JsonObject, String>() {
                    @Override
                    public String apply(@NonNull JsonObject jsonResultRequest) throws Exception {

                        /*
                        //Esta es otra forma de obtener la ciudad
                        //solo sería en caso de que el elemento compound_code fuera cambiado por parte de Google
                        //este método necesita una respuesta ok:  jsonResultRequest.get("status").getAsString().equals("OK")
                        //sin embargo compuund_code no necesita de una respuesta OK.
                         JsonArray results= jsonResultRequest.get("results").getAsJsonArray();
                         JsonArray address_components= results.get(0).getAsJsonObject().get("address_components").getAsJsonArray();
                         String ciudadByResults = address_components.get(0).getAsJsonObject().get("long_name").getAsString();
                        */
                        String plus_code=jsonResultRequest.get("plus_code").getAsJsonObject().get("compound_code").getAsString();
                        String ciudad=plus_code.split(",",2)[0].split(" ",2)[1];
                        String estadoPais = plus_code.split(",",2)[1];

                        String placeId= "";
                        if(jsonResultRequest.get("status").getAsString().equals("OK")){
                            JsonArray jResult = jsonResultRequest.get("results").getAsJsonArray();
                            if(jResult.size()>0){
                                if(jResult.get(0).getAsJsonObject().has("place_id"))
                                    placeId = "@"+ jResult.get(0).getAsJsonObject().get("place_id").getAsString();
                            }
                        }

                        String respuesta = ciudad+", "+estadoPais+placeId;

                        return respuesta;
                    }
                });
    }

    public Single<ArrayList<String>> getAutocomplete(Context context, String keyword, String type,
                                                     Map<String, String> queryParams)
            throws AException {
        if(queryParams == null) {
            queryParams = new HashMap<>();
        }
        queryParams.put("input", keyword);
        queryParams.put("type", type);

        return handle(context, ServiceRequestType.SearchPlaceAutocomplete, null, queryParams)
                .map(new Function<JsonObject, ArrayList<String>>() {
                    @Override
                    public ArrayList<String> apply(@NonNull JsonObject jsonObject) throws Exception {
                        ArrayList<String> results = new ArrayList<>();
                        for(int i = 0; i < jsonObject.get("predictions").getAsJsonArray().size(); i++) {
                            JsonObject prediction = jsonObject.get("predictions").getAsJsonArray()
                                                              .get(i).getAsJsonObject();
                            results.add(prediction.get("description").getAsString());
                        }
                        return results;
                    }
                });
    }

    public Single<List<String>> getDetailedAutocomplete(final Context context, String keyword, String type, Map<String, String> queryParams, final String sessionToken) throws AException{
        if(queryParams == null) {
            queryParams = new HashMap<>();
        }

        final Map <String, String > qParams =  queryParams;
        queryParams.put("input", keyword);
        queryParams.put("type", type);
        final String lang = queryParams.get("language");
        queryParams.put("sessiontoken", sessionToken);

        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(final ObservableEmitter<String> emitter) throws Exception {
                try {
                    handle(context, ServiceRequestType.SearchPlaceAutocomplete, null, qParams).map(new Function<JsonObject, JsonArray>() {
                         @Override
                         public JsonArray apply(@NonNull JsonObject jsonObject) {
                             return jsonObject.get("predictions").getAsJsonArray();
                         }
                     }).subscribe(new Consumer<JsonArray>() {
                        @Override
                        public void accept(@NonNull JsonArray predictionsArray) throws Exception {
                            List<JsonObject> predictionList = new ArrayList<>();

                            for (int i = 0; i < predictionsArray.size(); i++) {
                                JsonObject predictions = predictionsArray.get(i).getAsJsonObject();
                                predictionList.add(predictions);
                            }

                            Observable.fromIterable(predictionList).flatMap(new Function<JsonObject, ObservableSource<String>>() {
                                @Override
                                public ObservableSource<String> apply(@NonNull JsonObject predictions) throws Exception {
                                    String placeId = predictions.get("place_id").getAsString();
                                    String[] description = predictions.get("description").getAsString().split(",");
                                    /*String firstArg = description[0];
                                    String secondArg = "";
                                    if (description.length > 1)
                                        secondArg = description[1];*/
                                    Single<String> des = getListDescription(placeId, description[0], description[1], description[2]);
                                    Observable<String> temp = des.toObservable();
                                    return temp;//getFederatedAddress(context, placeId, lang, sessionToken).toObservable();
                                }
                            }).subscribe(new Consumer<String>() {
                                @Override
                                public void accept(@NonNull String s) throws Exception {
                                    emitter.onNext(s);
                                }
                            }, new Consumer<Throwable>() {
                                @Override
                                public void accept(@NonNull Throwable throwable) throws Exception {
                                    emitter.onError(throwable);
                                }
                            }, new Action() {
                                @Override
                                public void run() throws Exception {
                                    emitter.onComplete();
                                }
                            });
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            emitter.onError(throwable);
                        }
                    });
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        }).toList();

    }

    //Metodo para solo convertir de los elementos de descripcion y el id a un observable Single
    public Single<String> getListDescription(final String placeId, final String d1, final String d2, final String d3) {
        return Single.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                String res = d1 + ", " + d2 + ", " + d3 + ", " + placeId;
                return res;
            }
        });
    }

    public Single <String> getFederatedAddress(Context context, final String placeId, String language, String sessionToken) throws AException {
        Map<String, String> subQueryParams = new HashMap<String, String>();
        subQueryParams.put("placeid", placeId);
        if (language != null)
            subQueryParams.put("language", language);
        subQueryParams.put("sessiontoken", sessionToken);
        return handle(context, ServiceRequestType.GetDetails, null, subQueryParams)
                .map(new Function<JsonObject, String>() {
                    @Override
                    public String apply(@NonNull JsonObject jsonObject1) throws Exception {
                        JsonArray detailResult = jsonObject1.get("result").getAsJsonObject().get("address_components").getAsJsonArray();
                        String cityName = detailResult.get(0).getAsJsonObject().get("long_name").getAsString();
                        String stateName = detailResult.get(1).getAsJsonObject().get("long_name").getAsString();
                        String countryName = detailResult.get(2).getAsJsonObject().get("long_name").getAsString();
                        return cityName + ", " + stateName + ", "+ countryName + ", " + placeId;
                    }
                });
    }

    private Single<JsonObject> handle(Context context, ServiceRequestType service, JsonObject body, @NonNull Map<String, String> query) throws AException {
        query.put("key", this.mGoogleAPIKey);
        return RetrofitProxy.handleResponse(context, service, service.getServices(), body, query);
    }
}
