package veureka.pt.services.proxy.mapsengine;

import veureka.pt.services.proxy.retrofit.CredentialManager;

class MapsEngineCredential extends CredentialManager {
    
    private static String URL_BASE = "https://maps.googleapis.com/maps/api/";
    
    private static MapsEngineCredential sCredentials;
    
    private MapsEngineCredential() {
        super(null, null, URL_BASE);
    }
    
    public static MapsEngineCredential getInstance() {
        if(sCredentials == null) {
            sCredentials = new MapsEngineCredential();
        }
        return sCredentials;
    }
    
    @Override
    public void setUsername(String username) {
    }
    
    @Override
    public void setPassword(String password) {
    }
    
    @Override
    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }
}
