package veureka.pt.services.proxy.mapsengine;

import android.content.Context;

import veureka.pt.services.AException;
import veureka.pt.services.proxy.retrofit.IServiceRequestType;

enum ServiceRequestType implements IServiceRequestType<MapsEngineProxyEndPoint> {
    SearchPlaceAutocomplete("searchPlaceAutocomplete"),
    SearchGeocode("searchGeocode"),
    SearchDirections("searchDirections"),
    GetDetails("getDetails"),
    GetDistanceMatrix("getDistanceMatrix");

    private String service;
    private MapsEngineEndPoints endPoints;

    ServiceRequestType(String service) {
        this.service = service;
        this.endPoints = MapsEngineEndPoints.MapsEngineProxyEndPoint;
    }
    
    @Override
    public String getService() {
        return this.service;
    }

    @Override
    public MapsEngineProxyEndPoint getServices() {
        return endPoints.getServices();
    }

    @Override
    public MapsEngineProxyEndPoint startEndPoint(Context context, String username, String password, String baseURL)
            throws AException {
        return endPoints.startEndPoint(context, username, password, baseURL);
    }
    
    @Override
    public MapsEngineProxyEndPoint startEndPoint(Context context, String token, String baseURL) throws AException {
        return endPoints.startEndPoint(context, token, baseURL);
    }
    
}
